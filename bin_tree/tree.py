class Node:
    def __init__(self, value, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right

    def __repr__(self):
        return u"{0}".format(self.value)


class Tree:
    def __init__(self):
        self.root = None

    def get_root(self):
        return self.root

    def add(self, val):
        if self.root is None:
            self.root = Node(val)
        else:
            self._add(val, self.root)

    def _add(self, val, node):
        if val < node.value:
            if node.left is not None:
                self._add(val, node.left)
            else:
                node.left = Node(val)
        else:
            if node.right is not None:
                self._add(val, node.right)
            else:
                node.right = Node(val)

    def in_order(self, node):
        """
        Order LEFT VALUE RIGHT 
        """
        out = ""
        if node is not None:
            out += self.in_order(node.left)
            out += "{0} ".format(node.value)
            out += self.in_order(node.right)

        return out

    def min(self):
        pass

    def max(self):
        pass

    def find(self, value):
        pass

    def delete(self, value):
        parent, child = self.find(value)


if __name__ == '__main__':
    tree = Tree()

    '''
                    5
    1                           6
        2
            3
                4   
    
    '''

    tree.add(5)
    tree.add(1)
    tree.add(2)
    tree.add(3)
    tree.add(4)
    tree.add(6)
    tree.min()
    # print(tree.in_order(tree.root))
    assert tree.min() == 1
    assert tree.max() == 6
    tree.delete(5)

    print(tree.in_order(tree.root))
    assert tree.in_order(tree.root) == "1 2 3 4 6 "
